USE [Test]
GO
/****** Object:  Table [dbo].[tbl_clasficacion]    Script Date: 28/01/2021 09:36:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_clasficacion](
	[idClasificacion] [int] IDENTITY(1,1) NOT NULL,
	[NombreClasificacion] [nchar](100) NOT NULL,
 CONSTRAINT [PK_tbl_clasficacion] PRIMARY KEY CLUSTERED 
(
	[idClasificacion] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_incidencia]    Script Date: 28/01/2021 09:36:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_incidencia](
	[IdIncidencia] [int] IDENTITY(1,1) NOT NULL,
	[idPerfil] [int] NULL,
	[IdUsuario] [int] NULL,
	[fechaInc] [datetime] NULL,
	[IdClasificacion] [int] NULL,
	[Prioridad] [nchar](5) NULL,
	[Asunto] [nvarchar](1500) NULL,
	[HorasSoporte] [numeric](18, 0) NULL,
	[Estado] [nchar](50) NULL,
 CONSTRAINT [PK_tbl_incidencia] PRIMARY KEY CLUSTERED 
(
	[IdIncidencia] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_perfil]    Script Date: 28/01/2021 09:36:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_perfil](
	[IdPerfil] [int] IDENTITY(1,1) NOT NULL,
	[NombrePerfil] [nchar](500) NULL,
 CONSTRAINT [PK_tbl_perfil] PRIMARY KEY CLUSTERED 
(
	[IdPerfil] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tbl_usuario]    Script Date: 28/01/2021 09:36:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_usuario](
	[IdUsuario] [int] IDENTITY(1,1) NOT NULL,
	[NombreUsuario] [nchar](100) NOT NULL,
 CONSTRAINT [PK_tbl_usuario] PRIMARY KEY CLUSTERED 
(
	[IdUsuario] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[tbl_clasficacion] ON 

INSERT [dbo].[tbl_clasficacion] ([idClasificacion], [NombreClasificacion]) VALUES (1, N'Hardware                                                                                            ')
INSERT [dbo].[tbl_clasficacion] ([idClasificacion], [NombreClasificacion]) VALUES (2, N'Ofimática                                                                                           ')
INSERT [dbo].[tbl_clasficacion] ([idClasificacion], [NombreClasificacion]) VALUES (3, N'Sistema Operativo                                                                                   ')
INSERT [dbo].[tbl_clasficacion] ([idClasificacion], [NombreClasificacion]) VALUES (4, N'Otros                                                                                               ')
SET IDENTITY_INSERT [dbo].[tbl_clasficacion] OFF
SET IDENTITY_INSERT [dbo].[tbl_incidencia] ON 

INSERT [dbo].[tbl_incidencia] ([IdIncidencia], [idPerfil], [IdUsuario], [fechaInc], [IdClasificacion], [Prioridad], [Asunto], [HorasSoporte], [Estado]) VALUES (1, 1, 1, CAST(N'2020-12-03T10:16:00.000' AS DateTime), 1, N'Alta ', N'Problema en Equipo', CAST(1 AS Numeric(18, 0)), N'Resuelto                                          ')
INSERT [dbo].[tbl_incidencia] ([IdIncidencia], [idPerfil], [IdUsuario], [fechaInc], [IdClasificacion], [Prioridad], [Asunto], [HorasSoporte], [Estado]) VALUES (2, 1, 1, CAST(N'2020-12-03T12:04:00.000' AS DateTime), 2, N'Alta ', N'Activación de office', CAST(1 AS Numeric(18, 0)), N'Pendiente                                         ')
INSERT [dbo].[tbl_incidencia] ([IdIncidencia], [idPerfil], [IdUsuario], [fechaInc], [IdClasificacion], [Prioridad], [Asunto], [HorasSoporte], [Estado]) VALUES (3, 2, 2, CAST(N'2020-12-03T13:14:00.000' AS DateTime), 3, N'Alta ', N'Problemas con el sistema operativo', CAST(1 AS Numeric(18, 0)), N'Pendiente                                         ')
INSERT [dbo].[tbl_incidencia] ([IdIncidencia], [idPerfil], [IdUsuario], [fechaInc], [IdClasificacion], [Prioridad], [Asunto], [HorasSoporte], [Estado]) VALUES (4, 1, 1, CAST(N'2020-12-03T16:10:00.000' AS DateTime), 1, N'Alta ', N'Desconexión de red', CAST(0 AS Numeric(18, 0)), N'Resuelto                                          ')
INSERT [dbo].[tbl_incidencia] ([IdIncidencia], [idPerfil], [IdUsuario], [fechaInc], [IdClasificacion], [Prioridad], [Asunto], [HorasSoporte], [Estado]) VALUES (5, 3, 3, CAST(N'2020-12-04T10:20:00.000' AS DateTime), 2, N'Baja ', N'Problemas de compatibilidad con archivos de excel', CAST(2 AS Numeric(18, 0)), N'Resuelto                                          ')
INSERT [dbo].[tbl_incidencia] ([IdIncidencia], [idPerfil], [IdUsuario], [fechaInc], [IdClasificacion], [Prioridad], [Asunto], [HorasSoporte], [Estado]) VALUES (6, 1, 4, CAST(N'2020-12-04T11:53:00.000' AS DateTime), 1, N'Media', N'Monitor no enciende', CAST(3 AS Numeric(18, 0)), N'Resuelto                                          ')
INSERT [dbo].[tbl_incidencia] ([IdIncidencia], [idPerfil], [IdUsuario], [fechaInc], [IdClasificacion], [Prioridad], [Asunto], [HorasSoporte], [Estado]) VALUES (7, 2, 5, CAST(N'2020-12-05T09:55:00.000' AS DateTime), 3, N'Baja ', N'Windows no carga', CAST(1 AS Numeric(18, 0)), N'Resuelto                                          ')
INSERT [dbo].[tbl_incidencia] ([IdIncidencia], [idPerfil], [IdUsuario], [fechaInc], [IdClasificacion], [Prioridad], [Asunto], [HorasSoporte], [Estado]) VALUES (8, 2, 2, CAST(N'2020-12-05T11:12:00.000' AS DateTime), 2, N'Baja ', N'Word no carga', CAST(5 AS Numeric(18, 0)), N'Resuelto                                          ')
INSERT [dbo].[tbl_incidencia] ([IdIncidencia], [idPerfil], [IdUsuario], [fechaInc], [IdClasificacion], [Prioridad], [Asunto], [HorasSoporte], [Estado]) VALUES (9, 3, 6, CAST(N'2020-12-05T12:14:00.000' AS DateTime), 4, N'Baja ', N'Internet lento', CAST(4 AS Numeric(18, 0)), N'Resuelto                                          ')
INSERT [dbo].[tbl_incidencia] ([IdIncidencia], [idPerfil], [IdUsuario], [fechaInc], [IdClasificacion], [Prioridad], [Asunto], [HorasSoporte], [Estado]) VALUES (10, 1, 4, CAST(N'2020-12-05T14:07:00.000' AS DateTime), 4, N'Baja ', N'Acceso a carpetas compartidas', CAST(3 AS Numeric(18, 0)), N'Resuelto                                          ')
SET IDENTITY_INSERT [dbo].[tbl_incidencia] OFF
SET IDENTITY_INSERT [dbo].[tbl_perfil] ON 

INSERT [dbo].[tbl_perfil] ([IdPerfil], [NombrePerfil]) VALUES (1, N'Contador                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            ')
INSERT [dbo].[tbl_perfil] ([IdPerfil], [NombrePerfil]) VALUES (2, N'Financiero                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ')
INSERT [dbo].[tbl_perfil] ([IdPerfil], [NombrePerfil]) VALUES (3, N'Asistente Compras                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   ')
SET IDENTITY_INSERT [dbo].[tbl_perfil] OFF
SET IDENTITY_INSERT [dbo].[tbl_usuario] ON 

INSERT [dbo].[tbl_usuario] ([IdUsuario], [NombreUsuario]) VALUES (1, N'Pedro Lopez                                                                                         ')
INSERT [dbo].[tbl_usuario] ([IdUsuario], [NombreUsuario]) VALUES (2, N'Jose Martinez                                                                                       ')
INSERT [dbo].[tbl_usuario] ([IdUsuario], [NombreUsuario]) VALUES (3, N'Carlos Lopez                                                                                        ')
INSERT [dbo].[tbl_usuario] ([IdUsuario], [NombreUsuario]) VALUES (4, N'Arturo Cruz                                                                                         ')
INSERT [dbo].[tbl_usuario] ([IdUsuario], [NombreUsuario]) VALUES (5, N'Maria Perez                                                                                         ')
INSERT [dbo].[tbl_usuario] ([IdUsuario], [NombreUsuario]) VALUES (6, N'Juan Lopez                                                                                          ')
SET IDENTITY_INSERT [dbo].[tbl_usuario] OFF
ALTER TABLE [dbo].[tbl_incidencia]  WITH CHECK ADD  CONSTRAINT [FK_tbl_incidencia_tbl_clasficacion] FOREIGN KEY([IdClasificacion])
REFERENCES [dbo].[tbl_clasficacion] ([idClasificacion])
GO
ALTER TABLE [dbo].[tbl_incidencia] CHECK CONSTRAINT [FK_tbl_incidencia_tbl_clasficacion]
GO
ALTER TABLE [dbo].[tbl_incidencia]  WITH CHECK ADD  CONSTRAINT [FK_tbl_incidencia_tbl_incidencia] FOREIGN KEY([idPerfil])
REFERENCES [dbo].[tbl_perfil] ([IdPerfil])
GO
ALTER TABLE [dbo].[tbl_incidencia] CHECK CONSTRAINT [FK_tbl_incidencia_tbl_incidencia]
GO
ALTER TABLE [dbo].[tbl_incidencia]  WITH CHECK ADD  CONSTRAINT [FK_tbl_incidencia_tbl_usuario] FOREIGN KEY([IdUsuario])
REFERENCES [dbo].[tbl_usuario] ([IdUsuario])
GO
ALTER TABLE [dbo].[tbl_incidencia] CHECK CONSTRAINT [FK_tbl_incidencia_tbl_usuario]
GO
